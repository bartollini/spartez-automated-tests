# Review

## Bartosz Szulc

### Question 1

#### Read the specification and identify any aspects that is unclear or risky.

#####  Functionality

1. What is considered as editing profile? Editing profile details or changing password or both?

2. Would it be possible for an administrator to edit profile of non-writeable users?

3. Should administrator be able to update profile of other administrators?

4. Writable user will be able to edit its profile from Profile settings displayed in a menu next to his username in the top navigation pane, where will this option be available for administrator who wants to edit profile of users? Would he need to go to Manage Users and select an user he wants to edit to see the edit details and change password buttons?

5. Would previous username and/or email be valid for a short period of time to ease migration?

#####  Validation

1. What is considered a valid username? What should happen when a save button is clicked and the provided username is invalid and/or empty? Should a notification be displayed? Where should it appear? Should it appear below the username input?

2. What should happen if an user with given username already exists? Should the username be considered invalid and thus notification displayed?

3. What is considered a valid full name? What should happen when a save button is clicked and the provided full name is invalid and/or empty? Should a notification be displayed? Where should it appear? Should it appear below the full name input?

4. What should happen if an user with given full name already exists? We should allow users with same full names, shouldn’t we?

5. What should happen if an user with given email address already exists? Should the email be considered invalid and notification displayed? Where should it appear? Should it appear below the email input?

6. Are all the details required? Does user need to provide a valid username, full name and email address in order to proceed?

7. What if a email address or username or full name gets cleared by the user and he tried to save? Should we consider empty fields as empty or as not updated?

8. What should happen when the provided passwords don’t match? Should we display a notification? Where should it appear? Maybe below the invalid field?

9. What is considered a valid password? What should happen when a password is invalid? Should we display a notification? Where should it appear? Should it appear below the first invalid password input?

10. Should new unique properties, like email or username, be considered unavailable for other users until change confirmed or expired?

#####  Usability

1. Are the fields, username, full name, email, filled with current details by default?

2. Should the save button be enabled only when all the required fields are valid and disabled when at least one of the required fields is invalid and/or empty?

3. Would the

#####  Navigation

1. Given user changed username and/or full name and/or email, and clicked cancel, should we display a navigation confirmation? Shouldn’t we help user avoid situation when he clicks cancel link by a mistake?

#####  Confirmation

1. Shouldn’t we send a confirmation email to user confirmed (old) email address and wait for him to confirm the changes before applying them in the system?

2. Who should be responsible for confirming changes? The one who made them or the one who they are relevant to?

3. When user details and/or password change and are successfully saved, should we send a confirmation email to the user and wait for his approval? Should it be the case when the change is done by administrator? Shouldn’t the confirmation email be send to the administrator in that case? Shouldn’t the changes be pending until confirmed by whoever did the change?

4. Wouldn’t email sent every time a change is made be an overburden to administrator? Shouldn’t we implement a better mechanism that would avoid access to administration panel from non-administrators? What if we require admin to re-login every time he enters admin options and give a token that would allow him to make admin operations for a limited time?

5. Assuming we won’t send a confirmation email, should we logout user on successful change to his profile details and/or password? Should we do the same when administrator makes a change to an user profile? Should we log out the user on successful change? Should we notify him about changes made to his profile by displaying a notification?

6. Assuming we will send a confirmation email on details change, should we display a notification after the save button is clicked and change processed by the system? Where should it appear? Maybe at the top?

7. Should we send confirmation email in all the cases or just when user details are changed by the user itself? Assuming we will display a current password box in the password change modal, we don’t really need a third confirmation from user that he really requested the change.

#####  Notification

1. Shouldn’t user be informed about any changes to its profile details and/or password? Shouldn’t we send an email to the confirmed user email address? If user is logged in, shouldn’t we display a notification in Stash about confirmed changes to its profile?

#####  Security

1. Shouldn’t we include third field in the change password modal where user would put its current password? Mustn’t user provide his current password when he wants to change it? Otherwise we may cause potential security bridge where passwords of logged in users can be changed by other users who have access to their computers.

2. Shouldn’t the existing password field be displayed only when an user makes a change to its own profile not when a change is made by an administrator? Administrator may not know current password of an user.

3. Should we log out user on successful password change? Successful meaning, new password valid, saved, and if necessary, confirmed by the user.

### Question 2
#### Imagine that the Product Manager is about to leave on a month’s holiday, and the feature will be implemented and shipped while she is away. She is in a hurry and has to leave work to catch her flight in 10 minutes. Choose 3-5 questions that you believe are the most important for her to answer before she leaves. Justify your choices.

Given Product Manager has limited time, I would ask her these five questions,

Would it be possible for an administrator to edit profile of non-writeable users?
First of all, I personally think administrator should be able to edit profile of all the system users, including non-writeable. Otherwise we’re limiting the functionality, and we may find ourselves in an edge case, if administrator is not able to amend details of non-writeable users and the users themselves are not able to, who is?

Shouldn’t we send an email asking user to confirm the changes he made to his profile?
All the changes made to user profile, like editing username, full name or email, need to be secure. It may not be enough just to require user to login before he edits his profile. User may forgot to logout and leave a browser running with Stash. We need a two way confirmation system. We could send confirmation emails with generated expring confirmation links to users who requested changes. This would prevent people who have accessed profile of already login user to change its email address and use the new address to reset password.

Shouldn’t we include a third box in the change password modal for the current user password if the user making a change is not an administrator?
We also need to prevent insecure password changes. Maybe instead of sending confirmation emails, we should include a third box for the current password. Only if the field matches the current user password and new password is valid and has been repeated, the password can be changed by user.

Shouldn’t we require administrator to re-login whenever he wants to perform an administration action, like edit user details or change its password, and allow access to the options for a limited time by providing a secure token?
Sending confirmation emails and asking for current password may not work in all the cases.
Let’s consider administrators. They may need to manage many user accounts. Wouldn’t be an overburden for them to confirm all their actions by emails? Regarding password changes, how would they know users current passwords? In order to make the feature more secure for administrators we may consider a mechanism of re-validating administrator access. Whenever administrator wants to edit user details or change its password he needs to re-login, confirm his password. In order to ease the admin, each re-login will grant access to administration options for a limited time. This could be achieved by generating an expiring secure token.

Shouldn’t we notify user about confirmed changes made to his account (details and/or password) by sending an email to his confirmed email address and by displaying notification in Stash?
User details may be changes by the user itself or by the administrator. While the first case shouldn’t cause any issues, use knows what has been changed as he ordered and confirmed the changes, changes made by administrator and not properly propagated may cause issues. How would user know his email address changed if he wasn’t the one who made the changes? The system should notify user by changes made to his profile. Doesn’t matter whether the changes were made by him or by administrator. We could send an email to the user’s confirmed email address, and in case this is a new address, also send an email to the previous one. We could also display a notification in Stash. This might be nice touch for users who are logged in to Stash at the very moment change had been made.

Why other questions are less important to me?
Well, we probably already have validation rules we could reuse, as users had to be created prior to amendment.
As long as everyone is notified about changes, nothing blocks one administrator from making changes to other administrator accounts.
I doubt these are our first modals, and we probably already have best practices for implementing them, their navigation and usability.
As development team we can answer most of the remaining questions based on our experience. We can take a risk, as answers to these questions aren’t so severe, won’t impact the feature in the same way as answers to the five chosen questions.

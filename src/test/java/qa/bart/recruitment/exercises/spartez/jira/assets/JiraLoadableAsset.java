package qa.bart.recruitment.exercises.spartez.jira.assets;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

/**
 * I'm base for all Jira objects, keeping WebDriver instance and supporting loading
 */
public abstract class JiraLoadableAsset<T extends JiraLoadableAsset<T>> extends LoadableComponent<T> {

    protected final WebDriver driver;

    protected final ElementLocatorFactory elementLocatorFactory;

    protected JiraLoadableAsset(WebDriver driver, ElementLocatorFactory elementLocatorFactory) {
        this.driver = driver;
        this.elementLocatorFactory = elementLocatorFactory;
        PageFactory.initElements(elementLocatorFactory, this);
    }

    public abstract T clone();
}

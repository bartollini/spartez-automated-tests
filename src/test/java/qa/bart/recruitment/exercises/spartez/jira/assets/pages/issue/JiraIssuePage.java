package qa.bart.recruitment.exercises.spartez.jira.assets.pages.issue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import qa.bart.recruitment.exercises.spartez.jira.assets.modals.issue.JiraEditIssueModal;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.issue.search.JiraIssueSearchPage;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.menu.JiraDefaultMenuPage;
import qa.bart.recruitment.exercises.spartez.jira.domain.JiraIssue;

import java.net.URI;

import static com.google.common.truth.Truth.assertThat;
import static qa.bart.recruitment.exercises.spartez.jira.domain.builders.JiraIssueBuilder.buildJiraIssue;

/**
 * I represent a Jira Issue page where you can see all the Issue details
 */
public class JiraIssuePage extends JiraDefaultMenuPage<JiraIssuePage> {

    private static final String ISSUE_PAGE_URL_FORMAT = "/browse/%s";
    private final String jiraIssueKey;
    @FindBy(id = "summary-val")
    private WebElement summaryLabel;
    @FindBy(id = "key-val")
    private WebElement keyLabel;
    @FindBy(id = "edit-issue")
    private WebElement editIssueButton;

    public JiraIssuePage(WebDriver driver, ElementLocatorFactory elementLocatorFactory, URI jiraUri, String jiraIssueKey) {
        super(driver, elementLocatorFactory, jiraUri);
        this.jiraIssueKey = jiraIssueKey;
    }

    private String buildUrl() {
        return url(String.format(ISSUE_PAGE_URL_FORMAT, jiraIssueKey));
    }

    @Override
    protected void load() {
        driver.get(buildUrl());
    }

    @Override
    protected void isLoaded() throws Error {
        String url = driver.getCurrentUrl();
        assertThat(url).contains(buildUrl());
        assertThat(url).doesNotContain(JiraIssueSearchPage.JQL_PARAM);
        assertThat(summaryLabel).isNotNull();
        assertThat(keyLabel).isNotNull();
        assertThat(keyLabel.getText()).isEqualTo(jiraIssueKey);
    }

    public JiraIssue getJiraIssue() {
        return buildJiraIssue().withSummary(summaryLabel.getText()).withKey(keyLabel.getText()).build();
    }

    public JiraIssuePage editJiraIssue(JiraIssue jiraIssue) {
        editIssueButton.click();
        JiraEditIssueModal<JiraIssuePage> jiraEditIssueModal = new JiraEditIssueModal<>(driver, elementLocatorFactory, this);
        return jiraEditIssueModal.get().editJiraIssue(jiraIssue);
    }

    @Override
    public JiraIssuePage clone() {
        return new JiraIssuePage(driver, elementLocatorFactory, jiraUri, jiraIssueKey);
    }
}

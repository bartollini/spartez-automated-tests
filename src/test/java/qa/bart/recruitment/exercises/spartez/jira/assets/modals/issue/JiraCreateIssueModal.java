package qa.bart.recruitment.exercises.spartez.jira.assets.modals.issue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import qa.bart.recruitment.exercises.spartez.jira.assets.modals.JiraLoadableModal;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.JiraLoadablePage;
import qa.bart.recruitment.exercises.spartez.jira.domain.JiraIssue;

import static com.google.common.truth.Truth.assertThat;

/**
 * I represent a page that appears when you want to create an issue in Jira
 */
public class JiraCreateIssueModal<T extends JiraLoadablePage<T>> extends JiraLoadableModal<JiraCreateIssueModal<T>, T> {

    @FindBy(id = "summary")
    private WebElement summaryBox;
    @FindBy(id = "create-issue-submit")
    private WebElement submitButton;

    public JiraCreateIssueModal(WebDriver driver, ElementLocatorFactory elementLocatorFactory, T parent) {
        super(driver, elementLocatorFactory, parent);
    }

    @Override
    protected void isLoaded() throws Error {
        assertThat(summaryBox).isNotNull();
        assertThat(submitButton).isNotNull();
    }

    public T createJiraIssue(JiraIssue jiraIssue) {
        summaryBox.clear();
        summaryBox.sendKeys(jiraIssue.getSummary());
        submitButton.submit();
        return parent();
    }

    @Override
    public JiraCreateIssueModal<T> clone() {
        return new JiraCreateIssueModal<>(driver, elementLocatorFactory, parent);
    }
}

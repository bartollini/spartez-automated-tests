package qa.bart.recruitment.exercises.spartez.jira.environment.browser.builders;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import qa.bart.recruitment.exercises.spartez.jira.helpers.properties.PropertiesReaderHelper;

import java.util.InvalidPropertiesFormatException;

/**
 * I buildWebDriverFromProperties WebDriver instance based on properties extracted from maven
 */
public class WebDriverPropertiesBuilder {

    private static final String BROWSER_NAME_KEY = "browser.name";

    public static WebDriver buildWebDriverFromProperties() {
        try {
            return WebDriverStrategy.valueOf(PropertiesReaderHelper.getProperty(BROWSER_NAME_KEY).toUpperCase()).getInstance();
        } catch (InvalidPropertiesFormatException e) {
            return WebDriverStrategy.DEFAULT.getInstance();
        }
    }

    /**
     * I decide which instance of WebDriver should be built based on its name
     */
    private enum WebDriverStrategy {
        DEFAULT {
            @Override
            WebDriver getInstance() {
                return new FirefoxDriver();
            }
        },
        FIREFOX {
            @Override
            WebDriver getInstance() {
                return new FirefoxDriver();
            }
        },
        CHROME {
            @Override
            WebDriver getInstance() {
                return new ChromeDriver();
            }
        },
        SAFARI {
            @Override
            WebDriver getInstance() {
                return new SafariDriver();
            }
        },
        PHANTOMJS {
            @Override
            WebDriver getInstance() {
                DesiredCapabilities capabilities = DesiredCapabilities.phantomjs();
                capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[]{"--web-security=false", "--ignore-ssl-errors=true", "--ssl-protocol=any"});
                return new PhantomJSDriver(capabilities);
            }
        },
        INTERNETEXPLORER {
            @Override
            WebDriver getInstance() {
                return new InternetExplorerDriver();
            }
        };

        abstract WebDriver getInstance();
    }
}

package qa.bart.recruitment.exercises.spartez.jira.tests.issue.cases;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import qa.bart.recruitment.exercises.spartez.jira.tests.assets.categories.RecruitmentTests;
import qa.bart.recruitment.exercises.spartez.jira.tests.issue.JiraIssueTest;

import static com.google.common.truth.Truth.assertThat;

/**
 * I check if issue can be created in Jira
 */
public class JiraCreateIssueTest extends JiraIssueTest {

    @Before
    public void setUp() {
        login();
        setUpIssueWithRandomSummary();
    }

    @Test
    @Category(RecruitmentTests.class)
    public void shouldCreateIssue_WithSummaryOnly() {
        // When
        jiraIssue = jiraProjectPage.get().createJiraIssueAndOpen(issueWithRandomSummary).getJiraIssue();
        // Then
        assertThat(jiraIssue.getSummary()).isEqualTo(issueWithRandomSummary.getSummary());
    }
}
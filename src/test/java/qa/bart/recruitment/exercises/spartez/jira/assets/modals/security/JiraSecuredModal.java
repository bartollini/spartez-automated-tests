package qa.bart.recruitment.exercises.spartez.jira.assets.modals.security;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import qa.bart.recruitment.exercises.spartez.jira.assets.modals.JiraLoadableModal;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.JiraLoadablePage;

import static com.google.common.truth.Truth.assertThat;

/**
 * I login users to Jira and redirect them to their original destination
 */
public class JiraSecuredModal<T extends JiraLoadablePage<T>> extends JiraLoadableModal<JiraSecuredModal<T>, T> {

    private static final String LOGIN_PAGE_URL = "https://id.atlassian.com/login";
    @FindBy(id = "username")
    private WebElement usernameBox;
    @FindBy(id = "password")
    private WebElement passwordBox;
    @FindBy(id = "login-submit")
    private WebElement submitButton;

    public JiraSecuredModal(WebDriver driver, ElementLocatorFactory elementLocatorFactory, T parent) {
        super(driver, elementLocatorFactory, parent);
    }

    @Override
    protected void load() {
        driver.get(LOGIN_PAGE_URL);
    }

    @Override
    protected void isLoaded() throws Error {
        String url = driver.getCurrentUrl();
        assertThat(url).contains(LOGIN_PAGE_URL);
        assertThat(usernameBox).isNotNull();
        assertThat(passwordBox).isNotNull();
        assertThat(submitButton).isNotNull();
    }

    public T login(String username, String password) {
        usernameBox.clear();
        usernameBox.sendKeys(username);
        passwordBox.clear();
        passwordBox.sendKeys(password);
        submitButton.submit();
        return parent();
    }

    @Override
    public JiraSecuredModal<T> clone() {
        return new JiraSecuredModal<>(driver, elementLocatorFactory, parent);
    }
}

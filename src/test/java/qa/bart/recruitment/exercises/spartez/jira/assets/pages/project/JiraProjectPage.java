package qa.bart.recruitment.exercises.spartez.jira.assets.pages.project;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.issue.search.JiraIssueSearchPage;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.menu.JiraDefaultMenuPage;

import java.net.URI;

import static com.google.common.truth.Truth.assertThat;

/**
 * I represent project page in Jira
 */
public class JiraProjectPage extends JiraDefaultMenuPage<JiraProjectPage> {

    private static final String PROJECT_PAGE_URL_FORMAT = "/browse/%s";
    private static final String JIRA_ISSUE_SEPERATOR = "-";
    private final String projectKey;

    @FindBy(id = "pd-key")
    private WebElement projectDefinitionKeyLabel;

    @FindBy(className = "project-details")
    private WebElement projectDetailsArea;

    public JiraProjectPage(WebDriver driver, ElementLocatorFactory elementLocatorFactory, URI jiraUri, String projectKey) {
        super(driver, elementLocatorFactory, jiraUri);
        this.projectKey = projectKey;
    }

    private String buildUrl() {
        return url(String.format(PROJECT_PAGE_URL_FORMAT, projectKey));
    }

    @Override
    protected void load() {
        driver.get(buildUrl());
    }

    @Override
    protected void isLoaded() throws Error {
        String url = driver.getCurrentUrl();
        assertThat(url).contains(buildUrl());
        assertThat(url).doesNotContain(JiraIssueSearchPage.JQL_PARAM);
        assertThat(url).doesNotContain(projectKey + JIRA_ISSUE_SEPERATOR);
        assertThat(projectDetailsArea).isNotNull();
        assertThat(projectDefinitionKeyLabel).isNotNull();
        assertThat(projectDefinitionKeyLabel.getText()).isEqualTo(projectKey);
    }

    @Override
    public JiraProjectPage clone() {
        return new JiraProjectPage(driver, elementLocatorFactory, jiraUri, projectKey);
    }
}

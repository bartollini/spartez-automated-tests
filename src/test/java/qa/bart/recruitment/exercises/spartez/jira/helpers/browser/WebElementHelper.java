package qa.bart.recruitment.exercises.spartez.jira.helpers.browser;

import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * I help manage Web Driver elementss
 */
public class WebElementHelper {
    public static WebElement lastVisibleElement(List<WebElement> list) {
        for (int i = list.size() - 1; i > 0; i--) {
            WebElement element = list.get(i);
            if (element.isDisplayed()) {
                return element;
            }
        }
        return null;
    }
}

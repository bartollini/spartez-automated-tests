package qa.bart.recruitment.exercises.spartez.jira.tests.issue;

import com.google.guiceberry.junit4.GuiceBerryRule;
import com.google.inject.Inject;
import org.junit.Rule;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.issue.JiraIssuePage;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.issue.search.JiraIssueSearchPage;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.project.JiraProjectPage;
import qa.bart.recruitment.exercises.spartez.jira.domain.JiraIssue;
import qa.bart.recruitment.exercises.spartez.jira.environment.JiraTestEnvironment;
import qa.bart.recruitment.exercises.spartez.jira.environment.context.JiraTestContext;

import static qa.bart.recruitment.exercises.spartez.jira.tests.assets.mothers.JiraIssueMother.jiraIssueWithRandomSummary;

/**
 * I'm base of all Jira issue related tests
 */
public abstract class JiraIssueTest {

    @Rule
    public final GuiceBerryRule guiceBerry = new GuiceBerryRule(JiraTestEnvironment.class);

    @Inject
    protected JiraProjectPage jiraProjectPage;

    @Inject
    protected JiraIssueSearchPage jiraIssueSearchPage;
    protected JiraIssuePage jiraIssuePage;
    protected JiraIssue jiraIssue;
    protected JiraIssue issueWithRandomSummary;
    @Inject
    private JiraTestContext jiraTestContext;

    protected void login() {
        jiraProjectPage.get().login(jiraTestContext.getUsername(), jiraTestContext.getPassword());
    }

    protected void setUpIssueWithRandomSummary() {
        issueWithRandomSummary = jiraIssueWithRandomSummary();
    }

    protected void createIssueWithRandomSummary() {
        jiraIssuePage = jiraProjectPage.get().createJiraIssueAndOpen(issueWithRandomSummary);
        jiraIssue = jiraIssuePage.getJiraIssue();
    }
}

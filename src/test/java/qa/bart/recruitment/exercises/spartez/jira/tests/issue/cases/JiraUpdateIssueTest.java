package qa.bart.recruitment.exercises.spartez.jira.tests.issue.cases;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import qa.bart.recruitment.exercises.spartez.jira.tests.assets.categories.RecruitmentTests;
import qa.bart.recruitment.exercises.spartez.jira.tests.issue.JiraIssueTest;

import static com.google.common.truth.Truth.assertThat;

/**
 * I check if issue can be updated in Jira
 */
public class JiraUpdateIssueTest extends JiraIssueTest {

    @Before
    public void setUp() {
        login();
        setUpIssueWithRandomSummary();
        createIssueWithRandomSummary();
    }

    @Test
    @Category(RecruitmentTests.class)
    public void shouldUpdateIssue_WithSummaryOnly() {
        // When
        jiraIssue = jiraIssuePage.get().editJiraIssue(issueWithRandomSummary).getJiraIssue();
        // Then
        assertThat(jiraIssue.getSummary()).isEqualTo(issueWithRandomSummary.getSummary());
    }
}

package qa.bart.recruitment.exercises.spartez.jira.assets.pages.menu;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import qa.bart.recruitment.exercises.spartez.jira.assets.modals.issue.JiraCreateIssueModal;
import qa.bart.recruitment.exercises.spartez.jira.assets.modals.security.JiraSecuredModal;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.JiraLoadablePage;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.issue.JiraIssuePage;
import qa.bart.recruitment.exercises.spartez.jira.domain.JiraIssue;

import java.net.URI;

/**
 * I'm a base for all the page objects that navigation relies on Jira default menu
 */
public abstract class JiraDefaultMenuPage<T extends JiraDefaultMenuPage<T>> extends JiraLoadablePage<T> {

    private static final String DATA_CREATED_ISSUE_KEY_ATTRIBUTE_NAME = "data-issue-key";

    @FindBy(className = "issue-created-key")
    private WebElement createdIssueLink;
    @FindBy(className = "login-link")
    private WebElement loginLink;
    @FindBy(id = "create_link")
    private WebElement createIssueLink;

    protected JiraDefaultMenuPage(WebDriver driver, ElementLocatorFactory elementLocatorFactory, URI jiraUri) {
        super(driver, elementLocatorFactory, jiraUri);
    }

    public T login(String username, String password) {
        if (loginLink != null) {
            loginLink.click();
        }
        JiraSecuredModal<T> jiraSecuredModal = new JiraSecuredModal<>(driver, elementLocatorFactory, (T) this);
        return jiraSecuredModal.get().login(username, password);
    }

    T createJiraIssue(JiraIssue jiraIssue) throws SecurityException {
        if (createIssueLink != null) {
            createIssueLink.click();
            JiraCreateIssueModal<T> jiraCreateIssueModal = new JiraCreateIssueModal<>(driver, elementLocatorFactory, (T) this);
            return jiraCreateIssueModal.get().createJiraIssue(jiraIssue);
        } else {
            throw new SecurityException("User not logged in");
        }
    }

    public JiraIssuePage createJiraIssueAndOpen(JiraIssue jiraIssue) throws SecurityException {
        createJiraIssue(jiraIssue);
        JiraIssuePage jiraIssuePage = new JiraIssuePage(driver, elementLocatorFactory, jiraUri, extractCreateIssueKey());
        return jiraIssuePage.get();
    }

    private String extractCreateIssueKey() {
        return createdIssueLink.getAttribute(DATA_CREATED_ISSUE_KEY_ATTRIBUTE_NAME);
    }
}

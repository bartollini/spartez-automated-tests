package qa.bart.recruitment.exercises.spartez.jira.assets.exceptions;

/**
 * I'm raised when there are issues with Jira Issue Search due to network issues
 */
public class SearchTimeoutException extends Exception {
    public SearchTimeoutException(String message, Throwable cause) {
        super(message, cause);
    }
}

package qa.bart.recruitment.exercises.spartez.jira.environment.browser.configuration.builders;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import qa.bart.recruitment.exercises.spartez.jira.helpers.properties.PropertiesReaderHelper;

import java.util.InvalidPropertiesFormatException;

/**
 * I buildWebDriverFromProperties Elements Locator Factory based on properties extracted from Maven
 */
public class ElementLocatorFactoryPropertiesBuilder {

    private static final String AJAX_TIMEOUT_KEY = "browser.configuration.ajax.timeout";

    public static ElementLocatorFactory buildElemntLocatorFactoryFromProperties(WebDriver driver) {
        try {
            int timeout = Integer.parseInt(PropertiesReaderHelper.getProperty(AJAX_TIMEOUT_KEY));
            return new AjaxElementLocatorFactory(driver, timeout);
        } catch (InvalidPropertiesFormatException e) {
            return new DefaultElementLocatorFactory(driver);
        }
    }
}

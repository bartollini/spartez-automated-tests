package qa.bart.recruitment.exercises.spartez.jira.assets.modals;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import qa.bart.recruitment.exercises.spartez.jira.assets.JiraLoadableAsset;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.JiraLoadablePage;

/**
 * I'm abstract for all modals that need to have parents to exist
 * As modals we don't support chaining
 */
public abstract class JiraLoadableModal<T extends JiraLoadableModal<T, V>, V extends JiraLoadablePage<V>> extends JiraLoadableAsset<T> {

    protected final V parent;

    protected JiraLoadableModal(WebDriver driver, ElementLocatorFactory elementLocatorFactory, V parent) {
        super(driver, elementLocatorFactory);
        this.parent = parent;
    }

    @Override
    protected void load() {
    }

    protected V parent() {
        return parent.get();
    }
}

package qa.bart.recruitment.exercises.spartez.jira.environment.context;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * I have all the required information about System Under Test
 */
public class JiraTestContext {

    private URI jiraUri;
    private String projectKey = "TST";
    private String username = "admin";
    private String password = "admin";
    private int searchTimeout = 60;

    public JiraTestContext() throws URISyntaxException {
        jiraUri = new URI("http://127.0.0.1:8090/");
    }

    public JiraTestContext(URI jiraUri, String projectKey, String username, String password, int searchTimeout) {
        this.jiraUri = jiraUri;
        this.projectKey = projectKey;
        this.username = username;
        this.password = password;
        this.searchTimeout = searchTimeout;
    }

    public URI getJiraUri() {
        return jiraUri;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getSearchTimeout() {
        return searchTimeout;
    }
}

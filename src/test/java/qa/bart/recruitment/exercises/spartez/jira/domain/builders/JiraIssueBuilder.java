package qa.bart.recruitment.exercises.spartez.jira.domain.builders;

import qa.bart.recruitment.exercises.spartez.jira.domain.JiraIssue;

/**
 * I buildWebDriverFromProperties Jira Issue objects
 */
public class JiraIssueBuilder {

    private final JiraIssue jiraIssue = new JiraIssue();

    public static JiraIssueBuilder buildJiraIssue() {
        return new JiraIssueBuilder();
    }

    public JiraIssueBuilder withSummary(String summary) {
        jiraIssue.setSummary(summary);
        return this;
    }

    public JiraIssueBuilder withKey(String key) {
        jiraIssue.setKey(key);
        return this;
    }

    public JiraIssue build() {
        return jiraIssue;
    }
}

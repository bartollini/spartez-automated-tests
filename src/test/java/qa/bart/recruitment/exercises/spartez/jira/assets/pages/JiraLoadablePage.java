package qa.bart.recruitment.exercises.spartez.jira.assets.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import qa.bart.recruitment.exercises.spartez.jira.assets.JiraLoadableAsset;

import java.net.URI;

/**
 * I'm base for Jira pages, keeping information about Jira url
 * As pages we support chaining
 */
public abstract class JiraLoadablePage<T extends JiraLoadablePage<T>> extends JiraLoadableAsset<T> {

    protected final URI jiraUri;

    protected JiraLoadablePage(WebDriver driver, ElementLocatorFactory elementLocatorFactory, URI jiraUri) {
        super(driver, elementLocatorFactory);
        this.jiraUri = jiraUri;
    }

    protected String url(String subpage) {
        return jiraUri.resolve(subpage).toString();
    }
}

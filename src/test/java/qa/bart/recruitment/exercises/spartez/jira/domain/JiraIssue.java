package qa.bart.recruitment.exercises.spartez.jira.domain;

/**
 * I represent Jira Issue object
 */
public class JiraIssue {

    private String summary;
    private String key;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JiraIssue jiraIssue = (JiraIssue) o;

        if (key != null ? !key.equals(jiraIssue.key) : jiraIssue.key != null) return false;
        return !(summary != null ? !summary.equals(jiraIssue.summary) : jiraIssue.summary != null);

    }

    @Override
    public int hashCode() {
        int result = summary != null ? summary.hashCode() : 0;
        result = 31 * result + (key != null ? key.hashCode() : 0);
        return result;
    }
}

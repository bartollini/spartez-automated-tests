package qa.bart.recruitment.exercises.spartez.jira.assets.pages.issue.search;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import qa.bart.recruitment.exercises.spartez.jira.assets.exceptions.SearchTimeoutException;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.menu.JiraDefaultMenuPage;
import qa.bart.recruitment.exercises.spartez.jira.domain.JiraIssue;
import qa.bart.recruitment.exercises.spartez.jira.domain.builders.JiraIssueBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

/**
 * I represent page in Jira where you can search for a Jira issue of your choice
 */
public class JiraIssueSearchPage extends JiraDefaultMenuPage<JiraIssueSearchPage> {

    public static final String JQL_PARAM = "?jql=";
    public static final int SEARCH_START_TIMEOUT = 2;
    private static final String JIRA_ISSUE_LINK_KEY_CLASSNAME = "issue-link-key";
    private static final String JIRA_ISSUE_LINK_SUMMARY_CLASSNAME = "issue-link-summary";
    private static final String JIRA_ISSUE_SEARCH_CONTENT_CSS_SELECTOR = ".navigator-content:not(.pending)";
    private static final String JIRA_ISSUE_SEARCH_CONTENT_PENDING_CSS_SELECTOR = ".navigator-content.pending";
    private static final String JIRA_ISSUE_SEARCH_PAGE_URL = "/issues/";
    private final int searchTimeout;
    @FindBy(css = ".criteria-actions > button")
    private WebElement criteriaActionsButton;
    @FindBy(css = "[title='Issue Key Searcher']")
    private WebElement issueKeySearcherButton;
    @FindBy(css = "[title='Issue Key Searcher'] input")
    private WebElement issueKeySearcherCheckbox;
    @FindBy(css = "button[data-id='customfield_10180']")
    private WebElement issueKeySearcherField;
    @FindBy(css = "button[data-id='customfield_10180'] .searcherValue")
    private WebElement issueKeySearcherValueLabel;
    @FindBy(css = "#issue-filter input")
    private WebElement issueFilterBox;
    @FindBy(name = "filter")
    private WebElement filterButton;
    @FindBy(className = "search-title")
    private WebElement searchTitleLabel;
    @FindBy(id = "jql")
    private WebElement jqlField;
    @FindBy(css = ".issue-list > li")
    private List<WebElement> matchedJiraIssuesElements;

    public JiraIssueSearchPage(WebDriver driver, ElementLocatorFactory elementLocatorFactory, URI jiraUri, int searchTimeout) {
        super(driver, elementLocatorFactory, jiraUri);
        this.searchTimeout = searchTimeout;
    }

    @Override
    public JiraIssueSearchPage clone() {
        return new JiraIssueSearchPage(driver, elementLocatorFactory, jiraUri, searchTimeout);
    }

    @Override
    protected void load() {
        driver.get(url(JIRA_ISSUE_SEARCH_PAGE_URL));
    }

    @Override
    protected void isLoaded() throws Error {
        String url = driver.getCurrentUrl();
        assertThat(url).contains(JQL_PARAM);
        assertThat(searchTitleLabel).isNotNull();
        assertThat(criteriaActionsButton).isNotNull();
    }

    public JiraIssueSearchPage searchIssueByKey(String key) throws SearchTimeoutException {
        waitUntilSearchFinished();
        selectIssueKeySearcher(key);
        waitUntilIssueKeySearcherSet(key);
        return this;
    }

    private void selectIssueKeySearcher(String key) {
        selectIssueKeySearcher();
        issueFilterBox.clear();
        issueFilterBox.sendKeys(key);
        filterButton.submit();
    }

    private void waitUntilIssueKeySearcherSet(String key) throws SearchTimeoutException {
        try {
            new WebDriverWait(driver, searchTimeout).until(ExpectedConditions.textToBePresentInElement(issueKeySearcherValueLabel, key));
        } catch (Exception e) {
            throw new SearchTimeoutException("Search timed out, didn't set issue key searcher field", e);
        }
    }

    private void selectIssueKeySearcher() {
        criteriaActionsButton.click();
        if (issueKeySearcherCheckbox.isSelected()) {
            issueKeySearcherField.click();
        } else {
            issueKeySearcherButton.click();
        }
    }

    private void waitUntilSearchFinished() throws SearchTimeoutException {
        try {
            try {
                new WebDriverWait(driver, SEARCH_START_TIMEOUT).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(JIRA_ISSUE_SEARCH_CONTENT_PENDING_CSS_SELECTOR)));
            } catch (Exception e) {
            }
            new WebDriverWait(driver, searchTimeout).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(JIRA_ISSUE_SEARCH_CONTENT_CSS_SELECTOR)));
        } catch (Throwable e) {
            throw new SearchTimeoutException("Search timed out, didn't receive results", e);
        }
    }

    private JiraIssue transfortElementToJiraIssue(WebElement element) {
        String key = element.findElement(By.className(JIRA_ISSUE_LINK_KEY_CLASSNAME)).getText();
        String summary = element.findElement(By.className(JIRA_ISSUE_LINK_SUMMARY_CLASSNAME)).getText();
        return JiraIssueBuilder.buildJiraIssue().withKey(key).withSummary(summary).build();
    }

    public Collection<JiraIssue> getMatchedJiraIssues() throws SearchTimeoutException {
        waitUntilSearchFinished();
        Collection<JiraIssue> matchedJiraIssues = new ArrayList<>();
        if (matchedJiraIssuesElements != null) {
            for (WebElement matchedJiraIssueElement : matchedJiraIssuesElements) {
                JiraIssue jiraIssue = transfortElementToJiraIssue(matchedJiraIssueElement);
                matchedJiraIssues.add(jiraIssue);
            }
        }
        return matchedJiraIssues;
    }
}

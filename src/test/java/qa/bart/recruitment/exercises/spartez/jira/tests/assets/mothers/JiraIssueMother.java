package qa.bart.recruitment.exercises.spartez.jira.tests.assets.mothers;

import org.apache.commons.lang3.RandomStringUtils;
import qa.bart.recruitment.exercises.spartez.jira.domain.JiraIssue;

import static qa.bart.recruitment.exercises.spartez.jira.domain.builders.JiraIssueBuilder.buildJiraIssue;

/**
 * I give birth to different types of Jira issues
 */
public class JiraIssueMother {

    private static final int MAX_SUMMARY_LENGTH = 254;

    public static JiraIssue jiraIssueWithRandomSummary() {
        return buildJiraIssue().withSummary(RandomStringUtils.randomAlphabetic(MAX_SUMMARY_LENGTH)).build();
    }
}

package qa.bart.recruitment.exercises.spartez.jira.environment;

import com.google.common.testing.TearDown;
import com.google.common.testing.TearDownAccepter;
import com.google.guiceberry.GuiceBerryModule;
import com.google.guiceberry.TestScoped;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.issue.search.JiraIssueSearchPage;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.project.JiraProjectPage;
import qa.bart.recruitment.exercises.spartez.jira.environment.context.JiraTestContext;

import java.net.URISyntaxException;

import static qa.bart.recruitment.exercises.spartez.jira.environment.browser.builders.WebDriverPropertiesBuilder.buildWebDriverFromProperties;
import static qa.bart.recruitment.exercises.spartez.jira.environment.browser.configuration.builders.ElementLocatorFactoryPropertiesBuilder.buildElemntLocatorFactoryFromProperties;
import static qa.bart.recruitment.exercises.spartez.jira.environment.context.builders.JiraTestContextPropertiesBuilder.buildJiraTestContextFromProperties;

/**
 * I provide tests with various objects necessary for them, like Test Context or WebDriver instance
 */
public final class JiraTestEnvironment extends AbstractModule {

    @Override
    protected void configure() {
        install(new GuiceBerryModule());
    }

    @Provides
    @Singleton
    public JiraTestContext getJiraTestContext() throws URISyntaxException {
        return buildJiraTestContextFromProperties();
    }

    @Provides
    @TestScoped
    public ElementLocatorFactory getElementLocatorFactory(WebDriver driver) {
        return buildElemntLocatorFactoryFromProperties(driver);
    }

    @Provides
    @TestScoped
    public WebDriver getWebDriver(TearDownAccepter tda) {
        final WebDriver driver = buildWebDriverFromProperties();
        tda.addTearDown(new TearDown() {
            @Override
            public void tearDown() throws Exception {
                driver.close();
            }
        });
        return driver;
    }

    @Provides
    @TestScoped
    public JiraProjectPage getJiraProjectPage(WebDriver driver, ElementLocatorFactory elementLocatorFactory, JiraTestContext testContext) {
        return new JiraProjectPage(driver, elementLocatorFactory, testContext.getJiraUri(), testContext.getProjectKey());
    }

    @Provides
    @TestScoped
    public JiraIssueSearchPage getJiraIssueSearrchPage(WebDriver driver, ElementLocatorFactory elementLocatorFactory, JiraTestContext testContext) {
        return new JiraIssueSearchPage(driver, elementLocatorFactory, testContext.getJiraUri(), testContext.getSearchTimeout());
    }
}

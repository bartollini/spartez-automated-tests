package qa.bart.recruitment.exercises.spartez.jira.assets.modals.issue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import qa.bart.recruitment.exercises.spartez.jira.assets.modals.JiraLoadableModal;
import qa.bart.recruitment.exercises.spartez.jira.assets.pages.JiraLoadablePage;
import qa.bart.recruitment.exercises.spartez.jira.domain.JiraIssue;

import static com.google.common.truth.Truth.assertThat;

/**
 * I represent a page that appears when you want to edit an issue in Jira
 */
public class JiraEditIssueModal<T extends JiraLoadablePage<T>> extends JiraLoadableModal<JiraEditIssueModal<T>, T> {

    @FindBy(id = "summary")
    private WebElement summaryBox;
    @FindBy(id = "edit-issue-submit")
    private WebElement submitButton;

    public JiraEditIssueModal(WebDriver driver, ElementLocatorFactory elementLocatorFactory, T parent) {
        super(driver, elementLocatorFactory, parent);
    }

    @Override
    protected void isLoaded() throws Error {
        assertThat(summaryBox).isNotNull();
        assertThat(submitButton).isNotNull();
    }

    public T editJiraIssue(JiraIssue jiraIssue) {
        summaryBox.sendKeys(jiraIssue.getSummary());
        submitButton.submit();
        return parent();
    }

    @Override
    public JiraEditIssueModal<T> clone() {
        return new JiraEditIssueModal<>(driver, elementLocatorFactory, parent);
    }
}

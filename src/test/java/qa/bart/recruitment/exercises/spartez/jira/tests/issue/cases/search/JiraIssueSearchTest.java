package qa.bart.recruitment.exercises.spartez.jira.tests.issue.cases.search;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import qa.bart.recruitment.exercises.spartez.jira.assets.exceptions.SearchTimeoutException;
import qa.bart.recruitment.exercises.spartez.jira.domain.JiraIssue;
import qa.bart.recruitment.exercises.spartez.jira.tests.assets.categories.RecruitmentTests;
import qa.bart.recruitment.exercises.spartez.jira.tests.issue.JiraIssueTest;

import java.util.Collection;

import static com.google.common.truth.Truth.assertThat;

/**
 * I test search for Jira issue option
 */
public class JiraIssueSearchTest extends JiraIssueTest {

    @Before
    public void setUp() {
        login();
        setUpIssueWithRandomSummary();
        createIssueWithRandomSummary();
    }

    @Test
    @Category(RecruitmentTests.class)
    public void shouldMatchIssue_ByItsKey() throws SearchTimeoutException {
        // When
        jiraIssueSearchPage.get().searchIssueByKey(jiraIssue.getKey());
        // Then
        Collection<JiraIssue> matchedJiraIssues = jiraIssueSearchPage.getMatchedJiraIssues();
        assertThat(matchedJiraIssues.size()).isEqualTo(1);
        assertThat(matchedJiraIssues.iterator().next()).isEqualTo(jiraIssue);
    }
}

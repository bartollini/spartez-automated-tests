package qa.bart.recruitment.exercises.spartez.jira.environment.context.builders;

import qa.bart.recruitment.exercises.spartez.jira.environment.context.JiraTestContext;
import qa.bart.recruitment.exercises.spartez.jira.helpers.properties.PropertiesReaderHelper;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.InvalidPropertiesFormatException;

/**
 * I buildWebDriverFromProperties Test Context based on properties extracted from Maven
 */
public class JiraTestContextPropertiesBuilder {

    private static final String JIRA_URL_KEY = "jira.url";
    private static final String JIRA_PROJECT_KEY = "jira.project.key";
    private static final String JIRA_USERNAME_KEY = "jira.username";
    private static final String JIRA_PASSWORD_KEY = "jira.password";
    private static final String JIRA_SEARCH_TIMEOUT_KEY = "jira.search.timeout";

    public static JiraTestContext buildJiraTestContextFromProperties() throws URISyntaxException {
        try {
            return new JiraTestContext(
                    new URI(PropertiesReaderHelper.getProperty(JIRA_URL_KEY)),
                    PropertiesReaderHelper.getProperty(JIRA_PROJECT_KEY),
                    PropertiesReaderHelper.getProperty(JIRA_USERNAME_KEY),
                    PropertiesReaderHelper.getProperty(JIRA_PASSWORD_KEY),
                    Integer.parseInt(PropertiesReaderHelper.getProperty(JIRA_SEARCH_TIMEOUT_KEY)));
        } catch (InvalidPropertiesFormatException | URISyntaxException e) {
            return new JiraTestContext();
        }
    }
}

package qa.bart.recruitment.exercises.spartez.jira.helpers.properties;

import java.io.IOException;
import java.net.URL;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

/**
 * I'm helping read property value from test.properties file
 */
public class PropertiesReaderHelper {
    private static final String TEST_PROPERTIES_FILENAME = "test.properties";

    private static final Properties properties = new Properties();

    static {
        final URL url = PropertiesReaderHelper.class.getClassLoader().getResource(TEST_PROPERTIES_FILENAME);
        try {
            assert url != null;
            properties.load(url.openStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(String key) throws InvalidPropertiesFormatException {
        String value = properties.getProperty(key);
        if (null == value || value.isEmpty()) {
            throw new InvalidPropertiesFormatException(String.format("Empty value for a property, %s", key));
        } else {
            return value;
        }
    }
}

# Prerequisites

1. Maven

2. Internet access

3. Access to http://jira.atlassian.com/

4. Web Browser, {firefox, phantomjs, internetexplorer, chrome, safari}, installed and configured to work with Selenium

# Execution

```bash
mvn clean test
```

# Configuration

There are several properties vital for the tests, they all are included in pom file, in properties section,

1. Browser Configuration

* browser.name=firefox - browser name {firefox, phantomjs, internetexplorer, chrome, safari}

* browser.configuration.ajax.timeout=60 - locating elements timeout

2. Jira Under Test Configuration

* jira.url=https://jira.atlassian.com/ - Jira instance under test

* jira.username=bart.szulc.recruitment@gmail.com - Jira user address email

* jira.password=SuperTajneHasloToJest - Jira user password

* jira.project.key=TST - Jira project under test

* jira.search.timeout=60 - Jira search issues timeout

3. Tests Configuration

* test.groups=qa.bart.recruitment.exercises.spartez.jira.tests.assets.categories.RecruitmentTests - group of unit tests to run